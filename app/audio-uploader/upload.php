<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 23/10/16
 * Time: 18:29
 */

require_once('vendor/autoload.php');
require_once('NonceUtil.php');
require_once('config.php');

// CHECK AND HANDLE FORM DATA

if(!empty($_POST)) {
    if (NonceUtil::check(NONCE_SECRET, filter_input(INPUT_POST, 'token', FILTER_SANITIZE_STRING))) {
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $names = filter_input(INPUT_POST, 'names', FILTER_SANITIZE_STRING);
        $location = filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING);
    } else {
        exit;
    }
}

if (!isset($email) || empty($email)) redirectError('email');
if (!isset($names) || empty($names)) redirectError('names');

$datetime = date('c');
$filename = uniqid();

$path = PATH_STORAGE . DIRECTORY_SEPARATOR . URLify::filter($datetime . '_' . trim(substr($names, 0, 20)));

mkdir($path);

// HANDLE FILE

$storage = new \Upload\Storage\FileSystem($path);
$file = new \Upload\File('audiofile', $storage);

$file->setName($filename);

$file->addValidations([
    new \Upload\Validation\Mimetype(['audio/ogg', 'audio/mpeg', 'audio/x-wav', 'audio/wav']),
    new \Upload\Validation\Size('30M')
]);

try {
    $file->upload();
} catch (\Exception $e) {
    redirectError('audiofile');
}

// SAVE FORM DATA

$fdata = fopen($path . '/data.json', 'x');
fwrite($fdata, json_encode([
    'email' => $email,
    'names' => $names,
    'location' => !empty($location) ? $location : null,
    'filename' => $file->getNameWithExtension(),
]));
fclose($fdata);

redirectSuccess();


function redirectError($err) {
    $data = serialize([
        'email' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING),
        'names' => filter_input(INPUT_POST, 'names', FILTER_SANITIZE_STRING),
        'location' => filter_input(INPUT_POST, 'location', FILTER_SANITIZE_STRING),
    ]);
    header(sprintf("Location: ./?error=%s&data=%s", $err, urlencode($data)));
    exit();
}

function redirectSuccess() {
    header("Location: ./?success=1");
    exit();
}