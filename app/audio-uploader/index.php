<?php

$success = filter_input(INPUT_GET, 'success', FILTER_VALIDATE_BOOLEAN);

$error = filter_input(INPUT_GET, 'error', FILTER_SANITIZE_STRING);
if (!empty($error)) {
    $data = filter_input(INPUT_GET, 'data', FILTER_SANITIZE_ENCODED);
    $data = unserialize(urldecode($data));
}
else $data = [];

function esc_attr($val) {
    return htmlspecialchars($val, ENT_QUOTES|ENT_HTML5, ini_get("default_charset"), false);
}

?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Appel pour réinventer Calais</title>

    <style>
        .success { color: green; font-weight: bold; }
        .error, span[aria-hidden] { color: red; }
        label > span {
            display: inline-block;
            min-width: 100px;
        }
        div {
            margin-bottom: 10px;
        }
        input {
            display: block;
        }
        button {
            margin-top: 10px;
        }
    </style>
</head>
<body>

<!--    <h1>Appel pour réinventer Calais</h1>-->

    <?php if(empty($success)):
        require_once('config.php');
        require_once('NonceUtil.php'); ?>

        <form id="form" action="upload.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?php echo NonceUtil::generate(NONCE_SECRET) ?>" />
            <div>
                <label>
                    <span>Courriel <span aria-hidden="true">*</span></span>
                    <input size="30" type="email" name="email" required value="<?php if (isset($data['email'])) echo esc_attr($data['email']); ?>" />
                </label>
                <?php if ($error === 'email'): ?>
                    <div class="error">Cette adresse est invalide, veuillez la vérifier.</div>
                <?php endif; ?>
                <div><small>
                    Votre adresse de courriel ne sera <strong>pas affichée</strong> publiquement, ni jamais cédée à un tiers.<br />
                    Nous l’utiliserons seulement pour vous recontacter si le fichier était illisible ou pour vous envoyer un nouvel appel sur reinventercalais.org.<br />
                    Nous retirons votre adresse de la liste sur <a href="mailto:contact@reinventercalais.org">simple demande</a>.
                </small></div>
            </div>

            <div>
                <label>
                    <span>Nom, prénom ou pseudonyme <span aria-hidden="true">*</span></span>
                    <input size="20" name="names" required value="<?php if (isset($data['names'])) echo esc_attr($data['names']); ?>" />
                </label>
                <?php if ($error === 'names'): ?>
                    <div class="error">Merci d’indiquer un nom ou un pseudonyme.</div>
                <?php endif; ?>
            </div>

            <div>
                <label>
                    <span>Lieu, ville ou pays</span>
                    <input size="20" name="location" value="<?php if (isset($data['location'])) echo esc_attr($data['location']); ?>" />
                </label>
            </div>

            <div>
                <label>
                    <span>Enregistrement audio (format mp3, ogg ou wav) <span aria-hidden="true">*</span></span>
                    <div><small>
                            Lien vers une page d’aide pour s’enregistrer... (penser windows, mac, ios, android)
                        </small></div>
                    <input id="audiofile" type="file" name="audiofile" required  value="" />
                </label>
                <div id="error" class="error">
                    <?php if ($error === 'audiofile'): ?>
                        Il y a un problème avec ce fichier : veuillez vérifier son format (mp3, ogg ou wav) et sa taille (max. 30 Mo).
                    <?php endif; ?>
                </div>
            </div>

            <div>
                <button type="submit">Envoyer</button>
            </div>
        </form>

        <script>
            (function () {
                var audiofile = document.getElementById('audiofile');
                var error = document.getElementById('error');

                document.getElementById('form').addEventListener('submit', function (event) {
                    var filesize = audiofile.files[0].size;

                    if (filesize > 30e6) { // 30MB
                        error.innerHTML = 'La taille du fichier est trop importante (doit être inférieure à 30 Mo). <br /> Compressez-le, puis de réessayer.';
                        event.preventDefault();
                        return false;
                    }
                });
            })();
        </script>

    <?php else: ?>
        <div class="success">
            Votre contribution a bien été enregistrée. Merci !
        </div>
    <?php endif; ?>
</body>
</html>