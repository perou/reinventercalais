<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 20/03/16
 * Time: 12:49
 */

namespace Plugins\Calais\Admin;


class Styles
{

    public function __construct()
    {
        add_action('admin_enqueue_scripts', [$this, 'loadAdminStyles']);
    }
    
    public function loadAdminStyles($hook)
    {
        wp_enqueue_style('calais-admin-styles', plugins_url('admin.css', __FILE__));
    }
}