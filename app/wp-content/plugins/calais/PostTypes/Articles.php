<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 13/03/16
 * Time: 01:19
 */

namespace Plugins\Calais\PostTypes;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Articles extends CustomPostType
{

    const NAME = 'calais_articles';

    public function createPostType()
    {
        // Kind of hide the built-in post type...
        register_post_type('post');
        register_post_type(self::NAME, [
            'labels' => [
                'name'                  => _x('Articles', 'post type general name', DOMAIN),
                'singular_name'         => _x('Article', 'post type singular name', DOMAIN),
                'menu_name'             => _x('Articles', 'admin menu', DOMAIN),
                'name_admin_bar'        => _x('Article', 'add new on admin bar', DOMAIN),
                'add_new'               => _x('Add New', 'article', DOMAIN),
                'add_new_item'          => __('Add New Article', DOMAIN),
                'new_item'              => __('New Article', DOMAIN),
                'edit_item'             => __('Edit Article', DOMAIN),
                'view_item'             => __('View Article', DOMAIN),
                'all_items'             => __('All Articles', DOMAIN),
                'search_items'          => __('Search Articles', DOMAIN),
                'not_found'             => __('No article found.', DOMAIN),
                'not_found_in_trash'    => __('No article found in Trash.', DOMAIN),
            ],
            'description' => __('Full-featured text article, with an abstract and inline pictures.', DOMAIN),
            'public'  => true,
            'has_archive' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-format-aside',
            'supports' => [
                'title',
                'editor',
                'revisions',
            ],
            'taxonomies' => [
                'category',
            ],
            'rewrite' => [
                'slug' => _x('articles', 'permalink’s slug', DOMAIN),
            ],
        ]);
    }

    function addFields()
    {
        Fields\Lead::Instance() -> register(self::NAME);
        Fields\Authors::Instance() -> register(self::NAME);
        Fields\Index::Instance() -> register(self::NAME);
        Fields\Plan::Instance() -> register(self::NAME);
        Fields\LeftToRight::Instance() -> register(self::NAME);
        Fields\Help\Articles::Instance() -> register(self::NAME);
    }

    public function filterContentFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;
        
        $abstract = get_field('calais-abstract');
        return $abstract . PHP_EOL . $content 
            . '<p><a href="' . get_permalink() . '">Lire l’article</a></p>';
    }
    
    public function filterExcerptFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;
        
        $abstract = get_field('calais-abstract');
        return $abstract;
    }

}