<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 13/03/16
 * Time: 01:25
 */

namespace Plugins\Calais\PostTypes;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


abstract class CustomPostType
{
    public function __construct()
    {
        add_action('init', [$this, 'createPostType'], 10);
        add_action('init', [$this, 'addFields'], 10);
        add_filter('the_content_feed', [$this, 'filterContentFeed'], 5);
        add_filter('the_excerpt_rss', [$this, 'filterExcerptFeed'], 5);
    }

    abstract public function createPostType();
    abstract public function addFields();
    abstract public function filterExcerptFeed($content);
    abstract public function filterContentFeed($content);
}