<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 13/03/16
 * Time: 00:51
 */

namespace Plugins\Calais\PostTypes;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Atlas extends CustomPostType
{

    const NAME = 'calais_atlas';

    function createPostType()
    {
        register_post_type(self::NAME, [
            'labels' => [
                'name'                  => _x('Atlas', 'post type general name', DOMAIN),
                'singular_name'         => _x('Atlas', 'post type singular name', DOMAIN),
                'menu_name'             => _x('Atlas', 'admin menu', DOMAIN),
                'name_admin_bar'        => _x('Atlas', 'add new on admin bar', DOMAIN),
                'add_new'               => _x('Add New', 'atlas', DOMAIN),
                'add_new_item'          => __('Add New Atlas', DOMAIN),
                'new_item'              => __('New Atlas', DOMAIN),
                'edit_item'             => __('Edit Atlas', DOMAIN),
                'view_item'             => __('View Atlas', DOMAIN),
                'all_items'             => __('All Atlas', DOMAIN),
                'search_items'          => __('Search Atlas', DOMAIN),
                'not_found'             => __('No atlas found.', DOMAIN),
                'not_found_in_trash'    => __('No atlas found in Trash.', DOMAIN),
            ],
            'description' => __('Two or four columns of pictures followed by a text. Each picture opens a full-width slideshow.', DOMAIN),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-screenoptions',
            'supports' => [
                'title',
                'revisions',
            ],
            'taxonomies' => [
                'category',
            ],
            'rewrite' => [
                'slug' => _x('atlas', 'permalink’s slug', DOMAIN),
            ],
        ]);
    }

    function addFields()
    {
        Fields\Atlas::Instance() -> register(self::NAME);
        Fields\Authors::Instance() -> register(self::NAME);
        Fields\Index::Instance() -> register(self::NAME);
        Fields\Help\Atlas::Instance() -> register(self::NAME);
    }

    public function filterContentFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;

        return '<h1>' . get_the_title_rss() . '</h1>' 
            . get_field('calais_atlas_description') 
            . '<p><a href="' . get_permalink() . '">Voir les photos</a></p>';
    }

    public function filterExcerptFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;

        return get_field('calais_atlas_description');
    }

}