<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class LeftToRight extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_575fff99e0b9f',
            'title' => _x('Options', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_5761923bdb2c4',
                    'label' => __('Langue', DOMAIN),
                    'name' => 'calais-lang',
                    'type' => 'text',
                    'instructions' => _x('fr, ar, en...', 'languages codes', DOMAIN),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => _x('fr', 'default language code', DOMAIN),
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => 2,
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_576192bfdb2c5',
                    'label' => __('Sens de lecture', DOMAIN),
                    'name' => 'calais-lang-direction',
                    'type' => 'radio',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'ltr' => __('→ De gauche à droite', DOMAIN),
                        'rtl' => __('← De droite à gauche', DOMAIN),
                    ),
                    'allow_null' => 0,
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => _x('ltr', 'default language direction', DOMAIN),
                    'layout' => 'vertical',
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}