<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Lead extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_56e5f41c6b2a7',
            'title' => _x('Chapeau', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_56e5f42744da0',
                    'label' => _x('Chapeau', 'field', DOMAIN),
                    'name' => 'calais-abstract',
                    'type' => 'textarea',
                    'instructions' => __('Résumé, accroche ou introduction...', DOMAIN),
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => 4,
                    'new_lines' => 'wpautop',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'seamless',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}