<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class CallForIdeas extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_575fff99e0b9b',
            'title' => __('Options', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_575fff9ce4574',
                    'label' => __('Appel à idées ?', DOMAIN),
                    'name' => 'call_for_ideas',
                    'type' => 'true_false',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => __('Appliquer le format des appels à idées.', DOMAIN),
                    'default_value' => 0,
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}