<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Fragments extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_56e6067960ef6',
            'title' => _x('Fragments', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_56ef0bd3017cc',
                    'label' => __('Lead Fragment', DOMAIN),
                    'name' => 'lead_fragment',
                    'type' => 'text',
                    'instructions' => __('See this first fragment as a kind of title. 50 characters max.', DOMAIN),
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => 50,
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_26e607290f594',
                    'label' => _x('Auteur ou référence', 'of fragment field', DOMAIN),
                    'name' => 'calais_lead_fragment_author',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => 50,
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => __('e.g.: Ali, Iran, facteur, 23 ans', DOMAIN),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_26e607290f595',
                    'label' => _x('Reporter', 'of fragment field', DOMAIN),
                    'name' => 'calais_lead_fragment_reporter',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => 50,
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => __('e.g.: John Doe', DOMAIN),
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                    'readonly' => 0,
                    'disabled' => 0,
                ),
                array (
                    'key' => 'field_56e6069b0f592',
                    'label' => _x('Following Fragments', 'field', DOMAIN),
                    'name' => 'fragments',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => 'field_56e606f40f593',
                    'min' => 1,
                    'max' => '',
                    'layout' => 'block',
                    'button_label' => __('Ajouter un fragment', DOMAIN),
                    'sub_fields' => array (
                        array (
                            'key' => 'field_56e606f40f593',
                            'label' => _x('Texte', 'of fragment field', DOMAIN),
                            'name' => 'calais_fragment_text',
                            'type' => 'textarea',
                            'instructions' => __('370 characters max.', DOMAIN),
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => 400,
                            'rows' => 4,
                            'new_lines' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_56e607290f594',
                            'label' => _x('Auteur ou référence', 'of fragment field', DOMAIN),
                            'name' => 'calais_fragment_author',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => 50,
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => __('e.g.: Ali, Iran, facteur, 23 ans', DOMAIN),
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_56e607290f595',
                            'label' => _x('Reporter', 'of fragment field', DOMAIN),
                            'name' => 'calais_fragment_reporter',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => 50,
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => __('e.g.: John Doe', DOMAIN),
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}