<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:11
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Index extends Field
{
    
    const WORD_TYPE = 'word';
    const IMAGE_TYPE = 'image';

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_56e5e5dbb45c9',
            'title' => _x('Index', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_570a724a086fd',
                    'label' => __('Tips about tiles shown on grids pages', DOMAIN),
                    'name' => 'calais_index_tips',
                    'type' => 'message',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'message' => sprintf('<p>%s</p><p>%s</p><p>%s</p>',
                        __('The <strong>first</strong> index defined down here will be displayed in grids pages (home page and categories’ indexes).', DOMAIN),
                        __('However, try to define at least two distinct indexes (one primary by <em>word</em>, and another primary by <em>image</em>), so one could easily remedy graphic appearance of those grids.', DOMAIN),
                        __('Drag’n’drop indexes using the left numeroted margin to reorder indexes.', DOMAIN)
                    ),
                    'new_lines' => '',
                    'esc_html' => 0,
                ),
                array (
                    'key' => 'field_56e5e783b6163',
                    'label' => __('Define Three Options to Show up this Entry in Indexes', DOMAIN),
                    'name' => 'calais_index',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => 'field_56e5e8bfb6164',
                    'min' => 1,
                    'max' => 3,
                    'layout' => 'block',
                    'button_label' => _x('Ajouter un élément', 'index option (not in use)', DOMAIN),
                    'sub_fields' => array (
                        array (
                            'key' => 'field_56e5e8bfb6164',
                            'label' => _x('Type', 'of index', DOMAIN),
                            'name' => 'calais_index_type',
                            'type' => 'radio',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => 25,
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array (
                                self::IMAGE_TYPE => _x('Image', 'type of index', DOMAIN),
                                self::WORD_TYPE => _x('Word', 'type of index', DOMAIN),
                            ),
                            'other_choice' => 0,
                            'save_other_choice' => 0,
                            'default_value' => 'word',
                            'layout' => 'horizontal',
                        ),
                        array (
                            'key' => 'field_56e5eae37b873',
                            'label' => _x('Catch Word', 'for index', DOMAIN),
                            'name' => 'calais_index_word',
                            'type' => 'text',
                            'instructions' => __('Word to display in a black & white tile.', DOMAIN),
                            'required' => 1,
                            'conditional_logic' => array (
                                array (
                                    array (
                                        'field' => 'field_56e5e8bfb6164',
                                        'operator' => '==',
                                        'value' => self::WORD_TYPE,
                                    ),
                                ),
                            ),
                            'wrapper' => array (
                                'width' => 37,
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => _x('Cook', 'example index word', DOMAIN),
                            'prepend' => '',
                            'append' => __('(max. 12 caractères)', DOMAIN),
                            'maxlength' => 12,
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_26e5eb3d7b874',
                            'label' => _x('Image', 'for word index', DOMAIN),
                            'name' => 'calais_index_word_image',
                            'type' => 'image',
                            'instructions' => __('This optional image will be shown on mouse hovering.', DOMAIN),
                            'required' => 0,
                            'conditional_logic' => array (
                                array (
                                    array (
                                        'field' => 'field_56e5e8bfb6164',
                                        'operator' => '==',
                                        'value' => self::WORD_TYPE,
                                    ),
                                ),
                            ),
                            'wrapper' => array (
                                'width' => 37,
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'id',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array (
                            'key' => 'field_56e5eb3d7b874',
                            'label' => _x('Image', 'for index', DOMAIN),
                            'name' => 'calais_index_image',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => array (
                                array (
                                    array (
                                        'field' => 'field_56e5e8bfb6164',
                                        'operator' => '==',
                                        'value' => self::IMAGE_TYPE,
                                    ),
                                ),
                            ),
                            'wrapper' => array (
                                'width' => 37,
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'id',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array (
                            'key' => 'field_56e5ebae7b875',
                            'label' => _x('Word', 'for image index', DOMAIN),
                            'name' => 'calais_index_image_title',
                            'type' => 'text',
                            'instructions' => __('This word will be shown on mouse hovering.', DOMAIN),
                            'required' => 1,
                            'conditional_logic' => array (
                                array (
                                    array (
                                        'field' => 'field_56e5e8bfb6164',
                                        'operator' => '==',
                                        'value' => self::IMAGE_TYPE,
                                    ),
                                ),
                            ),
                            'wrapper' => array (
                                'width' => 37,
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => _x('Cook', 'example index word', DOMAIN),
                            'prepend' => '',
                            'append' => __('(max. 12 caractères)', DOMAIN),
                            'maxlength' => 12,
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}