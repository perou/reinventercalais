<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Plan extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_575d3a349f282',
            'title' => __('Légendes des plans', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_575d3a47532a5',
                    'label' => _x('Plan', 'field', DOMAIN),
                    'name' => 'calais-plan',
                    'type' => 'repeater',
                    'instructions' => __('Légendes à afficher avec les plans', DOMAIN),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => 'field_575d3b12532a6',
                    'min' => 0,
                    'max' => '',
                    'layout' => 'row',
                    'button_label' => __('Ajouter un plan', DOMAIN),
                    'sub_fields' => array (
                        array (
                            'key' => 'field_575d3f7e8dbd9',
                            'label' => __('Plan', 'legend', DOMAIN),
                            'name' => 'calais-plan-hash',
                            'type' => 'text',
                            'instructions' => __('Nom de l’image correspondante', DOMAIN),
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => 'NOM_DU_FICHIER.ptif',
                            'prepend' => '#',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                        array (
                            'key' => 'field_575d40b611c50',
                            'label' => __('Légendes', 'field', DOMAIN),
                            'name' => 'calais-plan-legends',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 1,
                            'max' => '',
                            'layout' => 'row',
                            'button_label' => __('Ajouter une légende', DOMAIN),
                            'sub_fields' => array (
                                array (
                                    'key' => 'field_575d3b12532a6',
                                    'label' => __('Titre', 'legend', DOMAIN),
                                    'name' => 'calais-plan-legends-title',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 1,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => __('e.g.: Collectif Baya', 'legend', DOMAIN),
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => '',
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ),
                                array (
                                    'key' => 'field_575d3b8a532a7',
                                    'label' => __('Description', 'legend', DOMAIN),
                                    'name' => 'calais-plan-legends-description',
                                    'type' => 'textarea',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array (
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'maxlength' => '',
                                    'rows' => '',
                                    'new_lines' => 'wpautop',
                                    'readonly' => 0,
                                    'disabled' => 0,
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}