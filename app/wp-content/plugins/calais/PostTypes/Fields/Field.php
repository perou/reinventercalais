<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 23:19
 */

namespace Plugins\Calais\PostTypes\Fields;


abstract class Field
{
    private static $instances = [];

    public static function Instance()
    {
        $class_name = get_called_class();
        if (!isset(self::$instances[$class_name])) {
            self::$instances[$class_name] = new static;
        }
        return self::$instances[$class_name];
    }

    protected function __construct()
    {
        add_action('init', [$this, 'create'], 20);
    }

    protected $post_types = [];

    public function register($post_type)
    {
        $this -> post_types[] = [[
            'param' => 'post_type',
            'operator' => '==',
            'value' => $post_type,
        ]];
    }

    abstract public function create();
}