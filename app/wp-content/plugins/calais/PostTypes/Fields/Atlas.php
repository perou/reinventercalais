<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:58
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Atlas extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_56e5f5ea8b20c',
            'title' => _x('Atlas', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_51e5f6628b26f',
                    'label' => _x('Le format des images est', 'atlas field', DOMAIN),
                    'name' => 'calais_atlas_images_format',
                    'type' => 'radio',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'landscape' => __('à la Française (paysage)', 'atlas field', DOMAIN),
                        'portrait' => __('à l’Italienne (portrait)', 'atlas field', DOMAIN),
                    ),
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => 'landscape',
                    'layout' => 'horizontal',
                ),
                array (
                    'key' => 'field_56e5f6628b26f',
                    'label' => _x('Afficher les images en', 'atlas field', DOMAIN),
                    'name' => 'calais_atlas_layout',
                    'type' => 'radio',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        2 => __('deux colonnes', 'atlas field', DOMAIN),
                        4 => __('quatre colonnes', 'atlas field', DOMAIN),
                    ),
                    'other_choice' => 0,
                    'save_other_choice' => 0,
                    'default_value' => 2,
                    'layout' => 'horizontal',
                ),
                array (
                    'key' => 'field_56e5f6118b26e',
                    'label' => __('Galerie d’images', DOMAIN),
                    'name' => 'calais_atlas_gallery',
                    'type' => 'gallery',
                    'instructions' => __('Les images sont affichées en ligne (dans le sens de lecture) au nombre de deux ou quatre par ligne (selon le nombre de colonnes choisi plus haut). Ayez ce paramètre à l’esprit lorsque vous réordonnez vos images...', DOMAIN),
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'min' => '',
                    'max' => '',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array (
                    'key' => 'field_56e5f71a8b270',
                    'label' => _x('Complément textuel', 'atlas field', DOMAIN),
                    'name' => 'calais_atlas_description',
                    'type' => 'wysiwyg',
                    'instructions' => __('Le titre et ce texte apparaissent sous les images.', DOMAIN),
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'visual',
                    'toolbar' => 'basic',
                    'media_upload' => 0,
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'acf_after_title',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }

}