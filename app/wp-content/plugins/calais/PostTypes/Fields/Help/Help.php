<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 08/04/16
 * Time: 12:09
 */

namespace Plugins\Calais\PostTypes\Fields\Help;

use Plugins\Calais\PostTypes\Fields\Field;
use const Plugins\Calais\DOMAIN;

abstract class Help extends Field
{
    /**
     * @var array
     */
    protected $messages = [];

    /**
     * @var string
     */
    protected $hash;

    /**
     * @param string $title
     * @param string $msg
     */
    public function addMessage($title, $msg) {
        $hash = substr(sha1($this -> hash . $msg), 0, 13);
        $this -> messages[] = [
            'key' => 'field_' . $hash,
            'label' => $title,
            'name' => '',
            'type' => 'message',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => [ 
                'width' => '',
                'class' => 'calais-help-field',
                'id' => '',
            ],
            'message' => $msg,
            'new_lines' => '',
            'esc_html' => 0,
        ];
    }

    /**
     * @param string $title
     * @param string $filepath
     */
    public function embedSvg($title, $filepath) {
        if ($svg = file_get_contents($filepath))
            $this -> addMessage($title, $svg);
    }

    public function create() {
        acf_add_local_field_group(array (
            'key' => 'group_' . $this -> hash,
            'title' => _x('Tips', 'help field', DOMAIN),
            'fields' => $this -> messages,
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'side',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}