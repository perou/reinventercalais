<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 08/04/16
 * Time: 12:52
 */

namespace Plugins\Calais\PostTypes\Fields\Help;

use const Plugins\Calais\DOMAIN;

class Atlas extends Help
{
    protected $hash = '57077fbae14e4';

    public function register($post_type)
    {
        parent::register($post_type);
        $this -> embedSvg(_x('Layout Overview', 'help field', DOMAIN),
            plugin_dir_path(__FILE__) . '../../../assets/help/atlas.svg');
    }

}