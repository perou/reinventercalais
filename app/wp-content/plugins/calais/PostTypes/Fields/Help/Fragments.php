<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 08/04/16
 * Time: 12:52
 */

namespace Plugins\Calais\PostTypes\Fields\Help;

use const Plugins\Calais\DOMAIN;

class Fragments extends Help
{
    protected $hash = '570632c2b84df';

    public function register($post_type)
    {
        parent::register($post_type);
        $this -> embedSvg(_x('Layout Overview', 'help field', DOMAIN),
            plugin_dir_path(__FILE__) . '../../../assets/help/fragments.svg');
    }

}