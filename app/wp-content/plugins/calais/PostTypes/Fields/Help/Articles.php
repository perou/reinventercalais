<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 08/04/16
 * Time: 12:52
 */

namespace Plugins\Calais\PostTypes\Fields\Help;

use const Plugins\Calais\DOMAIN;

class Articles extends Help
{
    protected $hash = '57078020962a5';

    public function register($post_type)
    {
        parent::register($post_type);
        $this -> embedSvg(_x('Layout Overview', 'help field', DOMAIN),
            plugin_dir_path(__FILE__) . '../../../assets/help/article-text.svg');
    }

}