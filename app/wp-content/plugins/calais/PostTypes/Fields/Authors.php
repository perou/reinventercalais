<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 14/03/16
 * Time: 22:11
 */

namespace Plugins\Calais\PostTypes\Fields;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Authors extends Field
{

    public function create()
    {
        acf_add_local_field_group(array (
            'key' => 'group_56e58f5339844',
            'title' => _x('Authors', 'field', DOMAIN),
            'fields' => array (
                array (
                    'key' => 'field_56e5900ed0016',
                    'label' => __('List authors and people involved in the making process of the content', DOMAIN),
                    'name' => 'calais_authors',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => 'field_56e590c0d0017',
                    'min' => 1,
                    'max' => '',
                    'layout' => 'table',
                    'button_label' => __('Add an author', DOMAIN),
                    'sub_fields' => array (
                        array (
                            'key' => 'field_56e590c0d0017',
                            'label' => _x('Names, Functions', 'of author (field)', DOMAIN),
                            'name' => 'calais_author_names',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 1,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => __('e.g.: Jane Doe, Photograph', DOMAIN),
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                            'readonly' => 0,
                            'disabled' => 0,
                        ),
                    ),
                ),
            ),
            'location' => $this -> post_types,
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => 1,
            'description' => '',
        ));
    }
}