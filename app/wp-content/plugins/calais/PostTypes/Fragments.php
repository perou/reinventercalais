<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 13/03/16
 * Time: 01:02
 */

namespace Plugins\Calais\PostTypes;

use const Plugins\Calais\DOMAIN;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class Fragments extends CustomPostType
{

    const NAME = 'calais_fragments';

    public function createPostType()
    {
        add_action('save_post', [$this, 'setTitleFromLeadFragment']);

        register_post_type(self::NAME, [
            'labels' => [
                'name'                  => _x('Fragments', 'post type general name', DOMAIN),
                'singular_name'         => _x('Fragment', 'post type singular name', DOMAIN),
                'menu_name'             => _x('Fragments', 'admin menu', DOMAIN),
                'name_admin_bar'        => _x('Fragment', 'add new on admin bar', DOMAIN),
                'add_new'               => _x('Add New', 'Fragment', DOMAIN),
                'add_new_item'          => __('Add New Fragment', DOMAIN),
                'new_item'              => __('New Fragment', DOMAIN),
                'edit_item'             => __('Edit Fragment', DOMAIN),
                'view_item'             => __('View Fragment', DOMAIN),
                'all_items'             => __('All Fragments', DOMAIN),
                'search_items'          => __('Search Fragments', DOMAIN),
                'not_found'             => __('No Fragment found.', DOMAIN),
                'not_found_in_trash'    => __('No Fragment found in Trash.', DOMAIN),
            ],
            'description' => __('Quotes, dialogue or any sort of text fragment...', DOMAIN),
            'public' => true,
            'has_archive' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-editor-quote',
            'supports' => [
                'revisions',
            ],
            'taxonomies' => [
                'category',
            ],
            'rewrite' => [
                'slug' => _x('fragments', 'permalink’s slug', DOMAIN),
            ],
        ]);
    }

    public function setTitleFromLeadFragment($post_id) {
        if (get_post_type($post_id) !== self::NAME) return;
        if ($parent_id = wp_is_post_revision($post_id))
            $post_id = $parent_id;

        $title = get_field('lead_fragment', $post_id);
        
        $data = [
            'ID' => $post_id,
            'post_title' => $title,
        ];
        if (is_numeric(get_post_field('post_name', $post_id))) {
            $data['post_name'] = sanitize_title($title);
        }

        remove_action('save_post', [$this, 'setTitleFromLeadFragment']);

        wp_update_post($data);

        add_action('save_post', [$this, 'setTitleFromLeadFragment']);
    }

    function addFields()
    {
        Fields\Fragments::Instance() -> register(self::NAME);
        Fields\Index::Instance() -> register(self::NAME);
        Fields\Help\Fragments::Instance() -> register(self::NAME);
    }

    public function filterContentFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;

        $content = '<h1>';
        $content .= get_field('lead_fragment');
        if ($author = get_field('calais_lead_fragment_author'))
            $content .=  ' - ' . $author;
        if ($reporter = get_field('calais_lead_fragment_reporter'))
            $content .= ' - recueilli par ' . $reporter;
        
        $content .= '</h1><p><a href="' . get_permalink() . '">Lire les fragments suivants</a></p>';
        
        return $content;
    }

    public function filterExcerptFeed($content)
    {
        if (get_post_type() !== self::NAME) return $content;
        
        $content = get_field('lead_fragment');
        if ($author = get_field('calais_lead_fragment_author'))
            $content .=  ' - ' . $author;
        if ($reporter = get_field('calais_lead_fragment_reporter'))
            $content .= ' - recueilli par ' . $reporter;
        
        return $content;
    }

}