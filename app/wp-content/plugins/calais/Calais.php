<?php
/*
Plugin Name: Calais
Description: Add custom content types & logic Calais Plugin
Version:     1.0
Author:      Benjamin Menant
Author URI:  http://menant-benjamin.fr/
License:     WTFPL
License URI: http://www.wtfpl.net/txt/copying/
Domain Path: /languages
Text Domain: calais-plugin

Copyright © 2016 Benjamin Menant <menant-benjamin@menant-benjamin.fr>
This work is free software. It comes without any warranty, to the
extent permitted by applicable law. You can redistribute it and/or
modify it under the terms of the Do What The Fuck You Want To Public
License, Version 2, as published by Sam Hocevar. See the COPYING
file for more details.
*/

namespace Plugins\Calais;


defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


//
// Define plugin’s constants
//

// See "Text Domain" WP header
const DOMAIN = 'calais-plugin';


//
// Bootstrap the plugin!
//


register_activation_hook(__FILE__, __NAMESPACE__ . '\Plugin::activation');
register_deactivation_hook(__FILE__, __NAMESPACE__ . '\Plugin::deactivation');
add_action('plugins_loaded', __NAMESPACE__ . '\Plugin::bootstrap');

class Plugin
{

    static function activation() {
        spl_autoload_register(__CLASS__ . '::autoload');
        self::loadLanguages();
        self::definePostTypes(true);
        flush_rewrite_rules();
    }

    static function deactivation() {
        spl_autoload_register(__CLASS__ . '::autoload');
        flush_rewrite_rules();
    }

    static function bootstrap() {
        spl_autoload_register(__CLASS__ . '::autoload');

        self::loadLanguages();
        self::definePostTypes();
        self::registerFeed();
        self::loadAdminStyles();
        self::supportSvg();
    }
    
    protected static function loadAdminStyles() {
        new Admin\Styles();
    }

    protected static function registerFeed() {
        add_filter('request', function ($query) {
            if (isset($query['feed'])) {
                $query['post_type'] = [
                    PostTypes\Articles::NAME,
                    PostTypes\Atlas::NAME,
                    PostTypes\Fragments::NAME,
                ];
            }
            return $query;
        });
    }

    protected static function loadLanguages() {
        load_plugin_textdomain(DOMAIN, false, basename(dirname(__FILE__)) . '/languages');
    }
    
    protected static function definePostTypes($activation = false) {
        $articles = new PostTypes\Articles();
        $atlas = new PostTypes\Atlas();
        $fragments = new PostTypes\Fragments();

        PostTypes\Fields\CallForIdeas::Instance() -> register('page');
        
        if ($activation) {
            $articles -> createPostType();
            $atlas -> createPostType();
            $fragments -> createPostType();
        }
    }
    
    protected static function supportSvg() {
        add_action('wp_print_scripts', function () { wp_dequeue_script('bodhi_svg_inline'); }, 100);
    }

    /**
     * @param string $class_name
     */
    static function autoload($class_name) {
        $class_name = ltrim($class_name, '\\');
        if(strpos($class_name, __NAMESPACE__) !== 0) return;

        $class_name = str_replace(__NAMESPACE__, '', $class_name);

        $path = plugin_dir_path(__FILE__) .
            str_replace('\\', \DIRECTORY_SEPARATOR, $class_name) . '.php';

        require_once($path);
    }
}
