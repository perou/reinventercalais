<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Calais
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head <?php do_action('og_namespaces'); ?>>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'calais-theme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() || is_home() ) : ?>
				<h1 class="site-title">
					<span>
						<img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/logo.svg'); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>" />
                    </span>
				</h1>
			<?php else : ?>
				<p class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/logo.svg'); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>" />
					</a>
				</p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="sidebar-wrapper" aria-expanded="false">
				<?php esc_html_e( 'Navigation', 'calais-theme' ); ?>
				<div class="burger-wrapper">
					<span class="burger-bun-top"></span>
					<span class="burger-bun-filling"></span>
					<span class="burger-bun-bottom"></span>
				</div>
			</button>
			<div id="sidebar-wrapper" class="sidebar-wrapper">
                <?php
                    wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) );
                    get_sidebar();
                ?>
            </div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content<?php if (get_field('call_for_ideas', $post->ID)) echo ' call-for-ideas'; ?>">
