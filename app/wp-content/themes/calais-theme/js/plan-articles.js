/**
 * Created by bmenant on 09/04/16.
 */

(function () {
    'use strict';

    var options = {
        center: [0, 0],
        crs: L.CRS.Simple,
        zoom: 0,
        minZoom: 2,
        zoomControl: false,
        attributionControl: false
    };

    bootstrap(options);
    
    function bootstrap(options) {
        var hiresViewer_instance;

        document.getElementById('main')
            .addEventListener('click', function imgListener(evt) {
                
                if (/IMG/i.test(evt.target.nodeName) && evt.target.parentNode.hash)
                    imgClicked(evt.target.parentNode);
                   
                else if (evt.target.hash)
                    imgClicked(evt.target);
                
                function imgClicked(target) {
                    evt.preventDefault();
                    evt.stopPropagation();
                   
                    // First click only
                    hiresViewer_instance = hiresViewer_instance || new HiResViewer(options);

                    window.location = '#plan-viewer-controls';
                    hiresViewer_instance.open(target.hash);
                }
            }, false);
    }

    function HiResViewer(options) {
        this.options = options;
        this.ui = {
            root: document.getElementById('plan-viewer'),
            holder: null,
            legendHolder: document.getElementById('plan-viewer-legend'),
            legendCounter: document.getElementById('viewer-counter'),
            buttons: {}
        };
        this.arrImg = Array.prototype.slice.call( // Array.from() for dummies...
            document.querySelectorAll('#main .entry-content a[href^="#"] > img')
        ).map(function (img) { return img.parentNode.hash.substr(1) });
        this.iterator = 0;
        this.createUI();
        this.createMap();
    }
    
    HiResViewer.prototype.createMap = function createMap() {
        this.map = L.map(this.ui.holder, this.options);
        this.layer = null;
        this.legend = null;
    };
    
    HiResViewer.prototype.createUI = function createUI() {
        this.ui.root.innerHTML =
            '<div id="plan-viewer-holder" class="plan-viewer-holder"></div>' +
            '<div id="plan-viewer-controls" class="plan-viewer-controls">' +
                '<a id="plan-viewer-zoom-in" class="plan-viewer-zoom-in" href="">' +
                    '<div class="cross-1"></div>' +
                    '<div class="cross-2"></div>' +
                '</a>' +
                '<a id="plan-viewer-zoom-out" class="plan-viewer-zoom-out" href="">' +
                    '<div class="cross-1"></div>' +
                '</a>' + (this.arrImg.length > 1 ?
                    '<a id="plan-viewer-prev" class="plan-viewer-prev" title="Plan précédent" href="">' +
                        '<div class="cross-1"></div>' +
                        '<div class="cross-2"></div>' +
                    '</a>' +
                    '<a id="plan-viewer-next" class="plan-viewer-next" title="Plan suivant" href="">' +
                        '<div class="cross-1"></div>' +
                        '<div class="cross-2"></div>' +
                    '</a>' : '') +
                '<a id="plan-viewer-legend" class="plan-viewer-legend" href="" title="Afficher les annotations">' +
                    '<svg viewBox="0 0 60 60">' +
                        '<g transform="translate(50.914 -928.32)">' +
                            '<path d="m-20.579 967.94h-1.342v-12.749h-4.0261v-1.2302h5.3681v13.98zm-0.78285-19.236c0.44734 0 0.78285 0.11184 1.0065 0.44735 0.22367 0.22367 0.44734 0.67101 0.44734 1.0065 0 0.33551-0.11184 0.67102-0.44734 1.0065-0.22367 0.33551-0.67102 0.44735-1.0065 0.44735-0.44734 0-0.78285-0.11184-1.0065-0.44735-0.22367-0.22367-0.44734-0.55918-0.44734-1.0065 0-0.44734 0.11184-0.78285 0.44734-1.0065 0.33551-0.33551 0.67102-0.44735 1.0065-0.44735z"/>' +
                            '<path id="plan-viewer-legend-circle" d="m-20.914 928.32a30 30 0 0 0 -30 30 30 30 0 0 0 30 30 30 30 0 0 0 30 -30 30 30 0 0 0 -30 -30zm0 1.2426a28.757 28.757 0 0 1 28.757 28.757 28.757 28.757 0 0 1 -28.757 28.757 28.757 28.757 0 0 1 -28.757 -28.757 28.757 28.757 0 0 1 28.757 -28.757z"/>' +
                        '</g>' +
                    '</svg>' +
                '</a>' +
                '<a id="plan-viewer-close" class="plan-viewer-close" href="" title="Retourner sur l’article">' +
                    '<div class="cross-1"></div>' +
                    '<div class="cross-2"></div>' + 
                '</a>' +
            '</div>';
        this.ui.holder = document.getElementById('plan-viewer-holder');
        this.ui.buttons = {
            close: document.getElementById('plan-viewer-close'),
            zoomIn: document.getElementById('plan-viewer-zoom-in'),
            zoomOut: document.getElementById('plan-viewer-zoom-out'),
            next: document.getElementById('plan-viewer-next'),
            prev: document.getElementById('plan-viewer-prev'),
            legend: document.getElementById('plan-viewer-legend')
        };
        this.iifsrvUrl = 'https://pic.reinventercalais.org/iipsrv';
        document.getElementById('plan-viewer-controls')
            .addEventListener('click', this, false);
    };
    
    HiResViewer.prototype.launchMap = function launchMap(imgName) {
        this.iterator = this.arrImg.indexOf(imgName);
        this.layer = L.tileLayer.iiif(this.iifsrvUrl + '?IIIF=' + imgName + '/info.json');
        this.map.addLayer(this.layer);
        this.ui.currentLegend = document.getElementById('plan-viewer-legend-' + imgName) || null;
        this.hasLegend = !!this.ui.currentLegend;
        this.ui.legendCounter.textContent = (this.iterator + 1) + '/' + this.arrImg.length;
    };
    
    HiResViewer.prototype.open = function open(hash) {
        if (hash) {
            this.launchMap(hash.substr(1));
            this.ui.root.className =
                this.ui.root.className.replace('svg-viewer-disabled', '') +
                ' svg-viewer-enabled';
            window.addEventListener('popstate', this, false);
            document.body.className += ' no-scroll';
        }
    };

    HiResViewer.prototype.handleEvent = function HiResViewerEventHandler(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        if (evt.target === this.ui.buttons.zoomOut
        || evt.target.parentNode === this.ui.buttons.zoomOut) {
            this.map.zoomOut();
        }
        else if (evt.target === this.ui.buttons.zoomIn
        || evt.target.parentNode === this.ui.buttons.zoomIn) {
            this.map.zoomIn();
        }
        else if (evt.target === this.ui.buttons.next
            || evt.target.parentNode === this.ui.buttons.next) {
            this.next();
        }
        else if (evt.target === this.ui.buttons.prev
            || evt.target.parentNode === this.ui.buttons.prev) {
            this.prev();
        }
        else if (evt.target === this.ui.buttons.legend
            || evt.target.parentNode === this.ui.buttons.legend) {
            this.toggleLegend();
        }
        else {
            this.close();
        }
    };
    
    HiResViewer.prototype.toggleLegend = function toggleLegend() {
        if (this.legend) this.removeLegend();
        else {
            if (this.hasLegend) {
                this.legend = L.tileLayer.iiif(this.iifsrvUrl + '?IIIF=legends/' + this.arrImg[this.iterator] + '/info.json');
                this.map.addLayer(this.legend);
                this.ui.currentLegend.className += ' active';
            }
            else this.legend = true;
            
            this.ui.buttons.legend.className += ' active';
            this.ui.legendHolder.className += ' active';
        }
    };
    
    HiResViewer.prototype.removeLegend = function removeLegend() {
        if (this.hasLegend) {
            this.ui.currentLegend.className = this.ui.currentLegend.className.replace('active', '');
            this.map.removeLayer(this.legend);
        }
        this.legend = null;
        this.ui.buttons.legend.className = this.ui.buttons.legend.className.replace('active', '');
        this.ui.legendHolder.className = this.ui.legendHolder.className.replace('active', '');
    };
    
    HiResViewer.prototype.next = function next() {
        this.change(this.arrImg[(this.iterator + 1) % this.arrImg.length]);
    };
    
    HiResViewer.prototype.prev = function prev() {
        this.change(
            this.arrImg[this.iterator === 0
                ? (this.arrImg.length - 1)
                : (this.iterator - 1)]
        );
    };
    
    HiResViewer.prototype.change = function change(hash) {
        this.map.removeLayer(this.layer);
        if (this.legend) this.removeLegend();
        this.launchMap(hash);
    };
    
    HiResViewer.prototype.close = function close() {
        window.removeEventListener('popstate', this, false);
        this.ui.root.className = this.ui.root.className.replace('svg-viewer-enabled', '')
                                 + ' svg-viewer-disabled';
        setTimeout(this.map.removeLayer.bind(this.map, this.layer), 300);
        if (this.legend) setTimeout(this.removeLegend.bind(this), 300);
        document.body.className = document.body.className.replace('no-scroll', '');
    };

})();