/**
 * Created by bmenant on 09/04/16.
 */

(function () {
    'use strict';
    
    var options = {
        speed: 200
    };
    
    bootstrap(options);
    
    function bootstrap(options) {
        var swipe_instance = new Swiper(options);
        
        Array.from(document.getElementsByClassName('swipe-start-ancre'))
            .forEach(function forEachAncre(ancre) {
                ancre.addEventListener('click', function whenAncreClicked(evt) {
                    evt.preventDefault();
                    evt.stopPropagation();
                    swipe_instance.enable(this.dataset.id);
                }, false);
            });
    }

    function Swiper(options) {
        this.ui = {
            page: document.getElementById('page'),
            holder: document.getElementById('swipe'),
            close: document.getElementById('swipe-close'),
            next: document.getElementById('swipe-next'),
            prev: document.getElementById('swipe-prev')
        };
        this.root = new Swipe(this.ui.holder, options);
    }
    Swiper.prototype.enable = function SwiperEnable(pos) {
        this.addEventListeners();
        this.root.slide(Number(pos), 0);
        this.ui.holder.parentNode.className = 'swipe-overlay swipe-enabled';
    };
    Swiper.prototype.disable = function SwiperDisable() {
        this.removeEventListeners();
        this.ui.holder.parentNode.className = 'swipe-overlay swipe-disabled';
    }; 
    Swiper.prototype.addEventListeners = function SwiperAddEventListeners() {
        this.ui.page.addEventListener('click', this, false);
    };
    Swiper.prototype.removeEventListeners = function SwiperRemoveEventListeners() {
        this.ui.page.removeEventListener('click', this, false);
    };
    Swiper.prototype.handleEvent = function SwiperHandleEvent(evt) {
        evt.stopPropagation();
        if (evt.target.className !== 'swipe-image') {
            switch (evt.target.id) {
                case 'swipe-next':
                case 'swipe-next--':
                    evt.preventDefault();
                    this.root.next();
                    break;
                case 'swipe-prev':
                case 'swipe-prev--':
                    evt.preventDefault();
                    this.root.prev();
                    break;
                case 'swipe-close':
                case 'swipe-close--':
                    this.disable();
                    evt.preventDefault();
                    break;
                default:
                    this.disable();
                    break;
            }
        }
    };
    Swiper.prototype.destroy = function SwiperDestroy() {
        this.disable();
        this.root.kill.bind(this.root);
    }

})();