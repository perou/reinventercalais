<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 03/04/16
 * Time: 22:51
 */

//TODO: hook_title or something to set fragments’ title globally
$title = get_post_type() === Plugins\Calais\PostTypes\Fragments::NAME
    ? get_field('lead_fragment')
    : get_the_title();

$entry = get_field('calais_index')[0];
$type = $entry['calais_index_type'];
$classes = ["hentry-cover-$type"];

if ($type === Plugins\Calais\PostTypes\Fields\Index::IMAGE_TYPE) {
    $img = wp_get_attachment_image_src($entry['calais_index_image'], 'medium');
    $style = sprintf('background-image:url(%s)', esc_attr($img[0]));
}
elseif ($img = $entry['calais_index_word_image']) {
    $classes[] = 'hentry-cover-word-with-image';
    $img = wp_get_attachment_image_src($img, 'medium');
    $style = sprintf('background-image:url(%s)', esc_attr($img[0]));
}

?>

<article id="post-<?php the_ID(); ?>"
         <?php post_class($classes); ?>
         style="<?php if (isset($style)) echo $style; ?>">
    <a href="<?php echo esc_url(get_permalink()); ?>">
        <?php if ($type === Plugins\Calais\PostTypes\Fields\Index::WORD_TYPE): ?>
            <span><?php echo esc_html($entry['calais_index_word']); ?></span>
        <?php elseif($img_title = $entry['calais_index_image_title']): ?>
            <span><?php echo esc_html($img_title); ?></span>
        <?php endif; ?>
    </a>
    <time datetime="<?php echo get_the_date('c'); ?>"><?php echo get_the_date('d.m.y'); ?></time>
</article>
