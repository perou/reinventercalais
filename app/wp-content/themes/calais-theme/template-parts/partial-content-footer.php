<footer class="entry-calais-footer" dir="ltr" lang="fr">
    <address class="entry-authors">
        <?php $authors = get_field('calais_authors');
            if ($authors):
                foreach($authors as $i => $author):
                    if ($i) echo $i+1 < count($authors) ? ', ' : ' &amp; ';
                    ?><span><?php echo $author['calais_author_names']; ?></span><?php
            endforeach; endif; ?>,
    </address>
    <span class="entry-date">
        <time datetime="<?php echo esc_attr(get_the_date('c')); ?>"><?php
            echo 'le ' . get_the_date('j F Y'); 
        ?></time>.
    </span>
</footer>
