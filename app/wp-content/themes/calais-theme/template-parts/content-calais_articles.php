<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Calais
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>
	<?php if(get_field('calais-lang-direction')): ?>dir="<?php echo esc_attr(get_field('calais-lang-direction')); ?>"<?php endif; ?>
	<?php if(get_field('calais-lang')): ?>lang="<?php echo esc_attr(get_field('calais-lang')); ?>"<?php endif; ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

		?>
	</header><!-- .entry-header -->

	<div class="entry-lead">
		<?php if(get_field('calais-lang-direction') !== 'rtl'): ?><ul class="post-categories"><li>Considérant</li></ul><?php endif; ?>
        <div class="entry-abstract">
            <?php echo wptexturize(get_field('calais-abstract')); ?>
        </div>
		<?php get_template_part( 'template-parts/partial', 'content-footer'); ?> 
    </div>

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'calais-theme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
	</div><!-- .entry-content -->

	<div id="plan-viewer" class="svg-viewer-root"></div>
	<div id="plan-viewer-legend" class="plan-viewer-legends-wrapper">
		<h1><?php echo get_the_title(); ?> <span id="viewer-counter"></span></h1>
		<div class="plan-viewer-meta">
            <address class="entry-authors">
                <?php $authors = get_field('calais_authors');
                if ($authors):
                    foreach($authors as $i => $author):
                        if ($i) echo $i+1 < count($authors) ? ', ' : ' &amp; ';
                        ?><span><?php echo $author['calais_author_names']; ?></span><?php
                    endforeach; endif; ?>,
            </address>
            <span class="entry-date">
                <time datetime="<?php echo esc_attr(get_the_date('c')); ?>"><?php
                    echo 'le ' . get_the_date('j F Y');
                    ?></time>.
            </span>
        </div>
		<?php if(have_rows('calais-plan')): ?>
		<?php while(have_rows('calais-plan')): the_row(); ?>
        <div class="plan-viewer-legend-item" id="plan-viewer-legend-<?php echo esc_attr(get_sub_field('calais-plan-hash')); ?>">
            <?php while(have_rows('calais-plan-legends')): the_row(); ?>
                <h2><?php the_sub_field('calais-plan-legends-title'); ?></h2>
                <?php the_sub_field('calais-plan-legends-description'); ?>
            <?php endwhile; ?>
        </div>
        <?php endwhile; ?>
		<?php endif; ?>
	</div>
</article><!-- #post-## -->
