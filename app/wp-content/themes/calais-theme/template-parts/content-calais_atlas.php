<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Calais
 */

function atlas_picture_size($with4cols, $formatPortrait) {
    if ($with4cols && $formatPortrait) return 'thumbnail-portrait';
    if ($with4cols && !$formatPortrait) return 'thumbnail';
    if (!$with4cols && !$formatPortrait) return 'medium';
    if (!$with4cols && $formatPortrait) return 'medium-portrait';
}

$images = get_field('calais_atlas_gallery');
$with4cols = (int)get_field('calais_atlas_layout', false, false) === 4;
$formatPortrait = get_field('calais_atlas_images_format', false, false) === 'portrait';
$size = atlas_picture_size($with4cols, $formatPortrait);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		 ?>
	</header><!-- .entry-header -->

	<div class="entry-complement">
		<?php if ($desc = get_field('calais_atlas_description')): ?>
            <div class="entry-description">
                <?php echo wptexturize($desc) ?>
            </div>
		<?php endif;
			get_template_part( 'template-parts/partial', 'content-footer'); ?> 
    </div>

	<div class="entry-content">
		<?php if ($images): ?>
            <ul class="entry-atlas-<?php echo $with4cols ? 'four' : 'two'; ?>-columns">
                <?php foreach ($images as $key => $img):
                    $img_src = $img['sizes'][$size];
                    $img_width = $img['sizes'][$size . '-width'];
                    $img_height = $img['sizes'][$size . '-height']; ?>
                    <li><a data-id="<?php echo $key; ?>" href="<?php echo esc_url($img['sizes']['large']); ?>" class="swipe-start-ancre">
                        <img src="<?php echo esc_url($img_src); ?>" />
                    </a></li>
                <?php endforeach; ?>
            </ul>
		<?php endif; ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<?php if ($images): ?>
<div class="swipe-overlay">
<div id="swipe" class="swipe">
    <div class="swipe-wrapper">
        <?php foreach ($images as $l_img):
            $l_img_src = $l_img['sizes']['large'];
            $l_img_width = $l_img['sizes']['large-width'];
            $l_img_height = $l_img['sizes']['large-height']; ?>
            <div><img class="swipe-image" src="<?php echo esc_url($l_img_src); ?>" /></div>
        <?php endforeach; ?>
    </div>
    <a class="swipe-close swipe-control" id="swipe-close" href=""><img id="swipe-close--" src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/cross.svg'); ?>" alt="Fermer" /></a>
    <a class="swipe-next swipe-control" id="swipe-next" href=""><img id="swipe-next--" src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/arrow.svg'); ?>" alt="Suivante" /></a>
    <a class="swipe-prev swipe-control" id="swipe-prev" href=""><img id="swipe-prev--" src="<?php echo esc_url(get_stylesheet_directory_uri() . '/assets/arrow.svg'); ?>" alt="Précédente" /></a>
</div>
</div>
<?php endif; ?>
