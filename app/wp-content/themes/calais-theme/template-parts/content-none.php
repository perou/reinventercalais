<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Calais
 */

?>

<section class="no-results not-found">

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Encore aucune étude n’a été publiée', 'calais-theme' ); ?></h1>
			</header><!-- .page-header -->

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'calais-theme' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'calais-theme' ); ?></p>
			<?php
				get_search_form();

		else : ?>
			<header class="page-header entry-header">
				<h1 class="page-title entry-title">
					<?php esc_html_e(sprintf(
						'Aucune étude n’a encore été publiée par des %s',
						strtolower(apply_filters('get_the_archive_title', single_cat_title('', false)))
					), 'calais-theme'); ?>
				</h1>
			</header><!-- .page-header -->
			<div class="entry-content">
                <p>De nouvelles études sont publiées chaque semaine. Peut-être aurez-vous plus de chance lors de votre prochaine exploration ?</p>
            </div>
			<?php

		endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
