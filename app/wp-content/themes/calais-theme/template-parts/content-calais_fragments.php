<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Calais
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_single() ): ?>
            <h1 class="entry-fragment-lead"><?php the_field('lead_fragment'); ?></h1>
		<?php else: ?>
            <h2 class="entry-fragment-lead"><a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
                <?php the_field('lead_fragment'); ?>
            </a></h2>
		<?php endif; ?>
		<div class="entry-fragment-lead-footer">
            <div><?php the_field('calais_lead_fragment_author'); ?></div>
            <?php if ($reporter = get_field('calais_lead_fragment_reporter')): ?>
				<address>Rapporté par <?php echo $reporter; ?></address>
			<?php endif; ?>
        </div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php while(have_rows('fragments')): the_row('fragments'); ?>
			<?php $fragment = get_sub_field('calais_fragment_text');
				$length = mb_strlen($fragment);
				$class = 'entry-fragment ';
				if ($length < 175) {
					$class .= 'fragment-small';
				}
				else if ($length < 340) {
					$class .= 'fragment-medium';
				}
				else {
					$class .= 'fragment-large';
				}
			?>
			<section class="<?php echo $class; ?>">
                <p><?php echo $fragment; ?></p>
				<footer class="entry-fragment-footer">
					<div><?php the_sub_field('calais_fragment_author'); ?></div>
					<?php if ($reporter = get_field('calais_fragment_reporter')): ?>
                        <address>Rapporté par <?php echo $reporter; ?></address>
                    <?php endif; ?>
				</footer>
            </section>
		<?php endwhile; ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
