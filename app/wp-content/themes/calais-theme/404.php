<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Calais
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header entry-header">
					<h1 class="page-title entry-title"><?php esc_html_e( 'Page inexistante', 'calais-theme' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content entry-content">
					<p>Désolé, aucune étude ni aucune page n’a été trouvée à cette adresse.</p>
					<p><a href="/">Retourner à l’accueil</a></p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
