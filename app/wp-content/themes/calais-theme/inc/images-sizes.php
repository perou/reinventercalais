<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 03/04/16
 * Time: 16:40
 */

/**
 * Forces images cropping...
 */

update_option("thumbnail_crop", true, $autoload = false);

update_option("medium_crop", true, $autoload = false);

update_option("medium_large_crop", false, $autoload = false);

update_option("large_crop", false, $autoload = false);


/**
 * Sets up images resizing...
 */

update_option("thumbnail_size_w", 295, $autoload = false);
update_option("thumbnail_size_h", 215, $autoload = false);

update_option("medium_size_w", 615, $autoload = false);
update_option("medium_size_h", 450, $autoload = false);

update_option("medium_large_size_w", 1255, $autoload = false);
update_option("medium_large_size_h", 918, $autoload = false);

update_option("large_size_w", 1255, $autoload = false);
update_option("large_size_h", 918, $autoload = false);

/**
 * New image size
 */

add_image_size('thumbnail-portrait', 295, 404, true);
add_image_size('medium-portrait', 615, 840, true);
