<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 03/04/16
 * Time: 17:01
 */


function calais_theme_enqueue_swipe_script() {
    if (is_single() && get_post_type() === Plugins\Calais\PostTypes\Atlas::NAME) {
        wp_register_script(
            'calais-theme-swipe',
            get_template_directory_uri() . '/js/swipe.min.js',
            array(),
            '2.0.6',
            true
        );
        wp_enqueue_script(
            'calais-theme-swipe-control',
            get_template_directory_uri() . '/js/swipe-atlas.js',
            array('calais-theme-swipe'),
            '1.0.0',
            true
        );
    }
}
add_action( 'wp_enqueue_scripts', 'calais_theme_enqueue_swipe_script');
