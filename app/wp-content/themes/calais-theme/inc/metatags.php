<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 13/04/16
 * Time: 14:12
 */

// Metatags & Open Graph
add_action('wp_head',                   'calais_theme_display_metatags'         );
add_action('og_namespaces',             'calais_theme_display_og_namespaces'    );

remove_action('wp_head',                'rsd_link'                              );
remove_action('wp_head',                'wlwmanifest_link'                      );
remove_action('wp_head',                'rest_output_link_wp_head',         10,0);
remove_action('wp_head',                'print_emoji_detection_script',     7   );
remove_action('wp_head',                'wp_generator'                          );
remove_action('wp_print_styles',        'print_emoji_styles'                    );
remove_action('wp_head',                'feed_links_extra',                 2   );
remove_action( 'wp_head',               'wp_oembed_add_discovery_links'         );
remove_action( 'wp_head',               'wp_oembed_add_host_js'                 );

add_filter('feed_links_show_comments_feed', function () { return false; });

function calais_theme_display_metatags() {
    if (!is_admin()) {
        $meta = '<meta name="%s" content="%s">' . PHP_EOL;
        $og = '<meta property="%s" content="%s">' . PHP_EOL;
        
        if (is_singular()) {
            if ($desc = calais_theme_description())
                printf($meta, 'description', esc_attr($desc));

            if ($keywords = calais_theme_keywords())
                printf($meta, 'keywords', esc_attr($keywords));
            
            if ($desc) {
                printf($og, 'og:description', esc_attr($desc));
                printf($og, 'og:locale', esc_attr(get_locale()));
                printf($og, 'og:type', esc_attr(calais_theme_type()));
                printf($og, 'og:url', esc_url_raw(get_permalink()));
                printf($og, 'og:site_name', esc_attr(get_bloginfo('name')));
                printf($og, 'og:title', esc_attr(get_the_title()));
            }
            
            if ($img = calais_theme_img())
                printf($og, 'og:image', esc_url_raw($img));
        }
        if (is_front_page() || is_home()) {
            $desc = 'Prendre soin et non détruire ce que migrants, Calaisiens et bénévoles du monde entier ont construit '
                . 'ensemble, et entendre ce que la ville elle-même peut en tirer de forces et de richesses. Dépasser une '
                . 'situation de crise, incessamment décrite comme une impasse, en cultivant les ressources qui s’y '
                . 'découvrent jusqu’à tracer des perspectives urbaines tangibles, aujourd’hui insoupçonnées. Transformer '
                . 'l’ici afin que celles et ceux qui jusqu’alors y étaient acculés puissent s’inventer un ailleurs. Tels '
                . 'sont les enjeux de l’appel à idées « Réinventer Calais ».'; 
            
            printf($meta, 'description', esc_attr($desc));

            if ($keywords = calais_theme_keywords($take_all = true))
                printf($meta, 'keywords', esc_attr($keywords));

            printf($og, 'og:description', esc_attr($desc));
            printf($og, 'og:locale', esc_attr(get_locale()));
            printf($og, 'og:type', 'website');
            printf($og, 'og:url', esc_url_raw(get_bloginfo('url')));
            printf($og, 'og:site_name', esc_attr(get_bloginfo('name')));
            printf($og, 'og:title', esc_attr(get_the_title()));
        }
    }
}

function calais_theme_display_og_namespaces() {
    if (!is_admin()) {
        echo 'prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#"';
    }
}

function calais_theme_type() {
    switch (get_post_type()) {
        case Plugins\Calais\PostTypes\Articles::NAME:
        case Plugins\Calais\PostTypes\Atlas::NAME:
        case Plugins\Calais\PostTypes\Fragments::NAME:
            $type = 'article';
            break;
        default:
            $type = 'website';
            break;
    }
    return $type;
}

function calais_theme_img() {
    static $img;
    if (!isset($img)) {
        switch (get_post_type()) {
            case Plugins\Calais\PostTypes\Articles::NAME:
            case Plugins\Calais\PostTypes\Atlas::NAME:
            case Plugins\Calais\PostTypes\Fragments::NAME:
                $img = array_reduce(get_field('calais_index'), function ($acc, $item) {
                    if (empty($acc) && $item['calais_index_type'] === Plugins\Calais\PostTypes\Fields\Index::IMAGE_TYPE) {
                        return wp_get_attachment_image_src($item['calais_index_image'], 'full')[0];
                    }
                    return $acc;
                });
                break;
            default:
                $img = false;
                break;
        }
    }
    return empty($img) ? false : $img;

}

function calais_theme_description() {
    static $desc;
    if (!isset($desc)) {
        switch (get_post_type()) {
            case Plugins\Calais\PostTypes\Articles::NAME:
                $desc = 'Considérant ' . get_field('calais-abstract', null, false);
                break;
            case Plugins\Calais\PostTypes\Atlas::NAME:
                $desc = get_field('calais_atlas_description', null, false);
                break;
            case Plugins\Calais\PostTypes\Fragments::NAME:
                $desc = '';
                if ($author = get_field('calais_lead_fragment_author'))
                    $desc .= "Citation de $author. ";
                if ($reporter = get_field('calais_lead_fragment_reporter'))
                    $desc .= "Fragments recueillis par $reporter.";
                break;
            default:
                $desc = get_post() -> post_content;
                if(strlen($desc) > 500 )
                    $desc = substr($desc, 0, strpos($desc, ' ', 500)) . '...';
                $desc = apply_filters('get_the_excerpt', $desc);
                break;
        }
    }
    return empty($desc) ? false : wp_filter_nohtml_kses($desc);
}

function calais_theme_keywords($take_all = false) {
    static $keywords;
    if (!isset($keywords)) {
        if ($take_all) {
            $categories = get_categories();
        } 
        else {
            switch (get_post_type()) {
                case Plugins\Calais\PostTypes\Articles::NAME:
                case Plugins\Calais\PostTypes\Atlas::NAME:
                case Plugins\Calais\PostTypes\Fragments::NAME:
                    $categories = get_the_category();
                    break;
                default:
                    $categories = get_categories();
                    break;
            }
        }
        $keywords = array_reduce($categories, function ($acc, $item) {
            if (!empty($acc) && !empty($item))
                return $acc . ', ' . $item -> name;
            if (!empty($item))
                return $item -> name;
            return $acc;
        });
    }
    return empty($keywords) ? false : $keywords;
}
