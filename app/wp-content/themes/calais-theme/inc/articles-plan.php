<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 03/04/16
 * Time: 17:01
 */


function calais_theme_enqueue_plan_script() {
    if (is_single() && get_post_type() === Plugins\Calais\PostTypes\Articles::NAME) {
        wp_register_script(
            'jquery_cdn',
            'https://code.jquery.com/jquery-2.2.3.min.js',
            null,
            '2.2.3',
            true
        );
        wp_register_script(
            'leaflet_cdn',
            'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.js',
            null,
            '0.7.7',
            true
        );
        wp_register_script(
            'leaflet_iiif',
            get_template_directory_uri() . '/js/leaflet-iiif.min.js',
            ['leaflet_cdn', 'jquery_cdn'],
            '0.1.2',
            true
        );
        wp_enqueue_script(
            'calais-theme-plan-control',
            get_template_directory_uri() . '/js/plan-articles.js',
            ['leaflet_cdn', 'leaflet_iiif'],
            '1.0.0',
            true
        );
        wp_enqueue_style(
            'leaflet_cdn_style',
            'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/leaflet.css'
        );
    }
}
add_action( 'wp_enqueue_scripts', 'calais_theme_enqueue_plan_script');
