<?php
/**
 * Created by PhpStorm.
 * User: bmenant
 * Date: 03/04/16
 * Time: 17:01
 */

function calais_theme_index_tiles(WP_Query $query) {
    if (!$query->is_admin
    && $query->is_main_query()
    && ($query->is_home() || $query->is_archive())) { 
        calais_theme_index_types($query);
    }
}
add_action('pre_get_posts', 'calais_theme_index_tiles');

function calais_theme_index_types($query) {
    $query -> set('post_type', array(
        Plugins\Calais\PostTypes\Articles::NAME,
        Plugins\Calais\PostTypes\Atlas::NAME,
        Plugins\Calais\PostTypes\Fragments::NAME,
    ));
}