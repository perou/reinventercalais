#!/usr/bin/env bash

# Upgrade Packages
echo "Upgrading Ubuntu…"
apt-get update

# Install MySQL Server in a Non-Interactive mode. Default root password will be "root"
echo "Installing MySQL…"
echo "mysql-server mysql-server/root_password password root" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | sudo debconf-set-selections
apt-get -y install mysql-server

# Setup MySQL Database & Users
#echo "Preparing database…"
mysql -uroot -proot -e "CREATE DATABASE IF NOT EXISTS perou;"
mysql -uroot -proot -e "GRANT ALL ON perou.* TO 'perou'@'localhost' IDENTIFIED BY 'perou';"
mysql -uroot -proot -e "FLUSH PRIVILEGES;"

# Load Last Dump Database
export DB_BACKUP_FILEPATH=/vagrant/provision/backup.sql.gz
if [ -f $DB_BACKUP_FILEPATH ]; then
    echo "Loading existing database…"
    gunzip < $DB_BACKUP_FILEPATH | mysql -uroot -proot perou
fi

# Backup Database Every 30 minutes.
echo "*/30 * * * * mysqldump -uroot -proot perou | gzip > $DB_BACKUP_FILEPATH" | crontab

# Install Other Packages…
echo "Installing Apache, PHP & Bundler…"
apt-get -y install apache2 php5 php5-mysqlnd php5-apcu php5-gd php5-curl nfs-common

# Set up Apache
echo "Setting up Apache, PHP…"
sed -i '/DocumentRoot/ a\
        <Directory /var/www/html/>\
                AllowOverride All\
        </Directory>' /etc/apache2/sites-available/000-default.conf

a2enmod rewrite && service apache2 restart

# Set up PHP with development config…
rm -f /etc/php5/apache2/php.ini
ln -fs /usr/share/php5/php.ini-development /etc/php5/php.ini
ln -fs /etc/php5/php.ini /etc/php5/apache2/php.ini

# Map VirtualHost directory with synced folder
if ! [ -L /var/www/html ]; then
  rm -rf /var/www/html
  ln -fs /vagrant/app /var/www/html
fi

# Setup WordPress
cp -f /vagrant/provision/wp-config.php /vagrant/app/wp-config.php

# End
echo "Ready for action!"
