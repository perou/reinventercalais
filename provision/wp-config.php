<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'perou');

/** MySQL database username */
define('DB_USER', 'perou');

/** MySQL database password */
define('DB_PASSWORD', 'perou');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QH&r9KmsR6~#R7rW}^FT5Q9<1{j-<6<=Yu<+W#Lb-!8C|^.sO+Cqo=2i(8FIDDJf');
define('SECURE_AUTH_KEY',  'b_j$r5|F O[(i#wZ^y:-&iv[^oF6};nG]NOc+kM/paAuxBMg2%qfl2t0{^Hl|6+a');
define('LOGGED_IN_KEY',    'lL{-Bu)Q9A-j88;G32?*LMNC!nfYQUqg+l-SVpss1m+(|+/>queb>;jU`s)YK&|?');
define('NONCE_KEY',        'mnQCp?vX4[cz|#eGI$|z#&.-w0|KO-+j2#)Qwztgm,gK80Vz&Lcxv~;G.($sNXIt');
define('AUTH_SALT',        'eLuR?zLw7JMv^T]BvlfP05f7.#H;ETPW</G;H9]+k9Q:?XO F-PaBHs0Is~TF?KK');
define('SECURE_AUTH_SALT', 'Hf^v@t|`ldu$ V^K)f[!o41,KC|uO]78iK{i4A-WQQ48l&u66Xqxl-hmndnFuXk7');
define('LOGGED_IN_SALT',   'vg_P2Q%kqmizB7{RXP*!#3!s#Hmq1>xXTzCtaM}5^MDzm<1+O1WAY.==->tITi^q');
define('NONCE_SALT',       'BhXe6m-=^qk2V9->RMfM3I!+ M@8z80qb$7W$-f-%GA,5vuaI$mD$ygZ#y{mJYaY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
