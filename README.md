# Reinventer Calais

See reinventercalais.org.

## Licences

### Code
Copyright © 2016 Benjamin Menant <menant-benjamin@menant-benjamin.fr>
The **code** of the WordPress plugin & theme called “Calais” are free. 
You can redistribute them and/or modify them under the terms of the 
Do What The Fuck You Want To Public License, Version 2, as published
by Sam Hocevar. See http://www.wtfpl.net/ for more details.

### Graphic
Copyright © 2016 [agraphmobile](http://www.atelier-malte-martin.net/).
Copy, derivative and redistribution of the graphic design, logotype,
icons and any visual elements are restricted.

### Webfonts
Webfonts have their own authors and copyrights. Reuse of those is
certainly not granted nor allowed.